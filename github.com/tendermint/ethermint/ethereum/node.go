package ethereum

import (
	"gopkg.in/urfave/cli.v1"

	ethUtils "github.com/ethereum/go-ethereum/cmd/utils"
	"github.com/ethereum/go-ethereum/eth"
	"github.com/ethereum/go-ethereum/node"
)

// 节点（Node）是主要的对象
type Node struct {
	node.Node
}

// New函数创建一个新的节点
func New(conf *node.Config) (*Node, error) {
	stack, err := node.New(conf)
	if err != nil {
		return nil, err
	}

	return &Node{*stack}, nil // nolint: vet
}

// Start函数开启底层节点，并且结束p2p服务
func (n *Node) Start() error {
	// start p2p server
	err := n.Node.Start()
	if err != nil {
		return err
	}

	// stop it
	n.Node.Server().Stop()

	return nil
}

// NewNodeConfig函数配置p2p以及网络层
// #不稳定的
func NewNodeConfig(ctx *cli.Context) *node.Config {
	nodeConfig := new(node.Config)
	ethUtils.SetNodeConfig(ctx, nodeConfig)

	return nodeConfig
}

// NewEthConfig函数配置以太坊服务
// #不稳定的
func NewEthConfig(ctx *cli.Context, stack *node.Node) *eth.Config {
	ethConfig := new(eth.Config)
	ethUtils.SetEthConfig(ctx, stack, ethConfig)

	return ethConfig
}
