# ethermint
整理的tendermint和ethermint代码。
ethermint已经修改更新到最新版本的abci接口。
目前可以单个tendermint和单个ethermint连接，交互没问题。多个tendermint交互没问题。
多个tendermint和ethermint交互存在问题，已知bug：1、p2p创建连接时读取超时。2、1问题可设置超时完成，但后续建立mconnection时数据读取有问题，可能协议有问题，需要排查。
### 代码阅读指南：

ethermint入口：
[github.com/tendermint/ethermint/cmd/ethermint/main.go](https://gitee.com/lymtics/ethermint/blob/master/github.com/tendermint/ethermint/cmd/ethermint/main.go)


abci实现接口：
[github.com/tendermint/ethermint/app/app.go](https://gitee.com/lymtics/ethermint/blob/master/github.com/tendermint/ethermint/app/app.go)


ethermint初始化文件：
[github.com/tendermint/ethermint/step](https://gitee.com/lymtics/ethermint/tree/master/github.com/tendermint/ethermint/setup)


以太坊接口：
[github.com/tendermint/ethermint/ethereum](https://gitee.com/lymtics/ethermint/tree/master/github.com/tendermint/ethermint/ethereum)
### build说明
建议go至少升级为1.10.0，[下载地址](https://studygolang.com/dl)


需要安装gcc,注意用mingw时一定使用64位的mingw，[安装参考](https://www.cnblogs.com/valor-xh/p/7371710.html)


GOPATH为系统配置go的工作目录


将所有文件覆盖本地GOPATH/src下的文件，至GOPATH/srcgithub.com/tendermint/ethermint/cmd/中运行


go install ./ethermint


GOPATH/bin中就有ethermint文件


至GOPATH/srcgithub.com/tendermint/tendermint/cmd/中运行


go install ./tendermint


GOPATH/bin中就有tendermint文件


### 运行命令
- ethermint:


初始化时可以运行
ethermint --datadir dir init gensis.json 初始化


此处--datadir dir指定dir目录为文件目录，init gensis.json（换成对应文件地址） 为用gensis.json初始化目录。也可以不初始化






可以直接ethermint运行


或者





指定目录和连接tendermint时可运行


ethermint --datadir dir --tendermint_addr tcp://172.17.0.2:46657


tendermint_addr 表示连接的tendermint地址。不带这个参数则默认本地46657端口


详见[http://ethermint.readthedocs.io/en/master/
](http://ethermint.readthedocs.io/en/master/)







- tendermint:


初始化时运行
tendermint init --home yourpath


运行，--home指定文件目录


tendermint node --home yourpath

启动或者


tendermint node --home yourpath --p2p.seeds ID@host:46656 --p2p.laddr tcp://0.0.0.0:46656 --rpc.laddr tcp://0.0.0.0:46657 --proxy_app tcp://127.0.0.1:46658


--p2p.seeds为需要连接的tendermint地址，--p2p.laddr为本机tendermint地址（开多台可更换端口），--rpc.laddr 为rpc地址（本地开多台需要更换端口），--proxy_app为ethermint连接地址（值为dummy时不连接ethermint）



参考[https://tendermint.readthedocs.io/en/master/tools/docker.html](https://tendermint.readthedocs.io/en/master/tools/docker.html)

### 版本说明
github.com/tendermint/abci 0.10.2

github.com/tendermint/tendermint  0.17.1

github.com/tendermint/tmlibs 0.7.1

github.com/tendermint/go-crypto 0.2.2

github.com/tendermint/ethermint 0.5.4 修改过

github.com/ethereum/go-ethereum f1.6.7 ethermint修改过的版本https://github.com/tendermint/go-ethereum.git
